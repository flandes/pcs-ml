
# about GMM, Naive Bayesian and E-M algo:

consider data coming from a Gaussian Mixture Model (GMM). You can do:
- Gaussian estimation (GMM): no lables, no clusters, only X and you try to its representation.
- Naive Bayesian: if you have labels, model each class as a GMM. Then you can fit the GMM for each class (K indep. problems). Then you can classify.
- E-M algo: you do not have the labels, but assume there are some. Then you do the same, but the Naive-Bayes step becomes the M (maximization) step, and you have to guess the classes at each iteration (E step)


Tricks and tips:
- numerical rounding errors happen, using log-probas can help.
- for EM: usually, the K-means initialization is expected to work better than random affectations (very bad a priori !)
- if data is actually a GMM, EM (Gaussian) should work fine (not shown)
- for MNIST, Bernoulli model is ok. You can do semi-supervised learning. (shown in `2EM_algorithm-Correction-Bernoulli-Only.ipynb`)
- for MNIST, doing a PCA and then a Gaussian EM on the components, should be ok (unfinished work) `2EM_algorithm-Correction-Gaussian-model-onPCAofMNist.ipynb`
- for MNIST, Gaussian model on pixels does not work (numerical issues, inf-> NaNs)  (can be seen by hacking a bit `2EM_algorithm-Correction-Bernoulli-Only-olderNotations (weird - Kmeans init seems worse than random init).ipynb`)

!! I had a sign error in my model  !!
