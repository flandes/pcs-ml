Attention mechanism: theory and some practice too.


- Motivations, a bit of history (NLP)
- The attention mechanism itself (self-attention, scaled dot-product) a.k.a. ``Attention is all you need’’
- How it all fits in a Transformer (Transformer-Encoder)
- Limitations & Solutions
- ConViT : Visual Transformers
- Do it yourself ! – an easy task

I save apart LoRA and other topic if they are picked by students.
